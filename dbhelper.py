import pymysql
import datetime

class DBHelper:
    def connect(self,database="crimemap"):
        return pymysql.connect('localhost','root','MyMy2701',database)

    def get_all_inputs(self):
        conn = self.connect()
        try:
            query = "select description from crimes;"
            cursor = conn.cursor()
            cursor.execute(query)
            return cursor.fetchall()
        finally:
            conn.close()
    
    def add_input(self,data):
        conn = self.connect()
        try:
            query = "insert into crimes (description) values (%s);"
            cursor = conn.cursor()
            cursor.execute(query,data)
            conn.commit()
        finally:
            conn.close()
    
    def clear_all(self):
        conn = self.connect()
        try:
            query = "delete from crimes;"
            cursor = conn.cursor()
            cursor.execute(query)
            conn.commit()
        finally:
            conn.close()
    
    def add_crime(self, category, date, latitude, longitude, description):
        conn = self.connect()
        try:
            query = "INSERT INTO crimes (category, date, latitude,longitude, description) \
                VALUES ('%s', '%s', '%s', '%s', '%s');"%\
                (category, date, latitude, longitude, description)
            cursor = conn.cursor()
            cursor.execute(query)
            conn.commit()
        except Exception as e:
            print(e)
        finally:
            conn.close()

    def get_all_crimes(self):
        conn = self.connect()
        try:
            query = "select latitude,longitude,date,category,description from crimes;"
            cursor = conn.cursor()
            cursor.execute(query)
            named_crimes = []
            for crime in cursor:
                named_crime={
                    'latitude': crime[0],
                    'longitude':crime[1],
                    'date': datetime.datetime.strftime(crime[2], '%Y-%m-%d'),
                    'category':crime[3],
                    'description': crime[4]
                }
                named_crimes.append(named_crime)
            return named_crimes
        finally:
            conn.close()
        
