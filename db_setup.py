import pymysql

conn = pymysql.connect('localhost','root','MyMy2701')
try:
    cursor = conn.cursor()
    sql = "CREATE DATABASE IF NOT EXISTS crimemap"
    cursor.execute(sql)
    sql = """CREATE TABLE IF NOT EXISTS crimemap.crimes(
        id int not null auto_increment,
        latitude float(10,6),
        longitude float(10,6),
        date datetime,
        category varchar(50),
        description varchar(1000),
        updated_at timestamp,
        primary key(id)
    )"""
    cursor.execute(sql)
    conn.commit()
finally:
    conn.close()